package main

import (
	"HelloWorld/internal/controllers"
	"flag"
	"github.com/joho/godotenv"
	"google.golang.org/grpc"
	"log"
	"net/http"
	"os"
)

type Config struct {
	GRPC GRPCConfig
}

type GRPCConfig struct {
	Host string
}

var url = flag.String("url", ":8081", "http service address")

func init() {
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
}

func main() {

	conf := NewConf()

	grpcConn, err := grpc.Dial(conf.GRPC.Host, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer grpcConn.Close()

	handler := controllers.NewBaseHandler(grpcConn)
	orderCreate := controllers.NewOrderHandler(grpcConn)
	userCreate := controllers.NewLoginHandler(grpcConn)

	http.Handle("/", corsHandler(handler))
	http.Handle("/order_create", corsHandler(orderCreate))
	http.Handle("/user_create", corsHandler(userCreate))

	err = http.ListenAndServe(*url, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}

}

func corsHandler(h http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "OPTIONS" {
			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		} else {
			h.ServeHTTP(w, r)
		}
	}
}

func NewConf() *Config {
	return &Config{
		GRPC: GRPCConfig{
			Host: getEnv("GRPC_HOST", "localhost:50051"),
		},
	}
}

func getEnv(key string, defaultVal string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}
	return defaultVal
}
