package main

import (
	migration "HelloWorld/db"
	"HelloWorld/internal/server"
	"HelloWorld/internal/service"
	pb "HelloWorld/proto"
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/joho/godotenv"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
)

const (
	port = ":50051"
)

type Config struct {
	DB DBConfig
}

type DBConfig struct {
	Username string
	Password string
	DBName   string
}

func init() {
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
}

func main() {

	conf := NewConf()

	conn, err := pgxpool.Connect(context.Background(), "postgres://"+conf.DB.Username+":"+conf.DB.Password+"@localhost:5432/"+conf.DB.DBName)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
		os.Exit(1)
	}
	defer conn.Close()

	err = migration.MigrateUp("postgres", "postgres://"+conf.DB.Username+":"+conf.DB.Password+"@localhost:5432/"+conf.DB.DBName+"?sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	order := service.NewOrder(conn)
	user := service.NewUser(conn)

	appServer := server.NewGrpcServer(order, user)

	s := grpc.NewServer()
	pb.RegisterHelloWorldServer(s, appServer)
	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func NewConf() *Config {
	return &Config{
		DB: DBConfig{
			Username: getEnv("POSTGRES_USER", ""),
			Password: getEnv("POSTGRES_PASSWORD", ""),
			DBName:   getEnv("POSTGRES_DB", ""),
		},
	}
}

func getEnv(key string, defaultVal string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}
	return defaultVal
}
