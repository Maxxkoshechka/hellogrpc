-- +migrate Up
CREATE TABLE orders (
     id SERIAL PRIMARY KEY,
     user_id int NOT NULL,
     amount int NOT NULL,
     description VARCHAR(100) NOT NULL,
     comments VARCHAR(100) NOT NULL,
     status VARCHAR(100) NOT NULL,
     created_at timestamp with time zone NOT NULL,
     updated_at timestamp with time zone NOT NULL,
     constraint orders_user_id_fk
        foreign key (user_id) references users(id)
);

-- +migrate Down
DROP TABLE orders;