-- +migrate Up
create table users
(
    id         serial
        constraint user_pk
            primary key,
    name varchar(100) not null,
    email varchar(100) not null,
    login varchar(100) not null,
    password varchar(100) not null,
    is_deleted boolean   default false             not null,
    created_at timestamp default CURRENT_TIMESTAMP not null,
    updated_at timestamp default CURRENT_TIMESTAMP not null
);

create unique index user_id_uindex
    on users (id);

-- +migrate Down
DROP INDEX IF EXISTS user_id_uindex;
DROP TABLE users;