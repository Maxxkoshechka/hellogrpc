package service

import (
	"HelloWorld/internal/models"
	"HelloWorld/internal/repository"
	pb "HelloWorld/proto"
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Order interface {
	CreateOrder(ctx context.Context, r *pb.OrderRequest) error
	UpdateOrder(ctx context.Context, r *pb.OrderRequest) error
	DeleteOrder(ctx context.Context, r *pb.OrderRequest) error
}

type order struct {
	conn      *pgxpool.Pool
	orderRepo models.OrderRepository
}

func NewOrder(conn *pgxpool.Pool) Order {
	orderRepo := repository.NewOrderRepo(conn)

	return order{
		conn:      conn,
		orderRepo: orderRepo,
	}
}

func (o order) CreateOrder(ctx context.Context, r *pb.OrderRequest) error {
	order := models.Order{
		UserID:      int(r.UserId.UserId),
		Amount:      int(r.Amount),
		Description: r.Description,
		Comments:    r.Comments,
		Status:      r.Status,
		CreatedAt:   r.CreatedAt.AsTime(),
		UpdatedAt:   r.UpdatedAt.AsTime(),
	}
	err := o.orderRepo.Create(ctx, &order)
	if err != nil {
		return err
	}
	return nil
}

func (o order) UpdateOrder(ctx context.Context, r *pb.OrderRequest) error {
	return nil
}

func (o order) DeleteOrder(ctx context.Context, r *pb.OrderRequest) error {
	return nil
}
