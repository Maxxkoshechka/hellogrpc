package service

import (
	"HelloWorld/internal/models"
	"HelloWorld/internal/repository"
	jwt "HelloWorld/internal/tools"
	pb "HelloWorld/proto"
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"golang.org/x/crypto/bcrypt"
	"log"
)

type User interface {
	CreateUser(ctx context.Context, r *pb.UserRequest) error
	UpdateUser(ctx context.Context, r *pb.UserRequest) error
	DeleteUser(ctx context.Context, r *pb.UserRequest) error
}

type user struct {
	conn     *pgxpool.Pool
	userRepo models.UserRepository
}

func NewUser(conn *pgxpool.Pool) User {
	userRepo := repository.NewUserRepo(conn)

	return user{
		conn:     conn,
		userRepo: userRepo,
	}
}

func (u user) CreateUser(ctx context.Context, r *pb.UserRequest) error {
	user := models.User{
		Name:      r.Name,
		Login:     r.Login,
		Email:     r.Email,
		Password:  r.Password,
		IsDeleted: r.IsDeleted,
		CreatedAt: r.CreatedAt.AsTime(),
		UpdatedAt: r.UpdatedAt.AsTime(),
	}
	err := u.loginUser(ctx, user)
	if err != nil {
		return err
	}
	return nil
}

func (u user) UpdateUser(ctx context.Context, r *pb.UserRequest) error {
	return nil
}

func (u user) DeleteUser(ctx context.Context, r *pb.UserRequest) error {
	return nil
}

func (u user) loginUser(ctx context.Context, user models.User) error {

	err := user.ValidateUserData()
	if err != nil {
		log.Printf("Received: %v", err)
		return err
	}
	userModel, err := u.userRepo.Select(ctx, "SELECT id, name, email, login, password, created_at, is_deleted FROM users where login = $1;", user.Login)

	if userModel != nil {

		if err != nil {
			log.Printf("Received: %v", err)
			return err
		}

		// Если пользователь найден то проверяем пароль и выдаем токен
		if !CheckPasswordHash(user.Password, userModel.Password) {
			log.Printf("Received: %v", err)
			return err
		}
	} else {
		// Если пользователь не найден то регестрируем его
		userModel, err = signUp(ctx, &user, u.userRepo)
		if err != nil {
			log.Printf("Received: %v", err)
			return err
		}
	}

	_, tokenErr := jwt.CreateJWT(userModel)
	if tokenErr != nil {
		log.Printf("Received: %v", tokenErr)
		return err
	}
	return nil
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func signUp(ctx context.Context, userModel *models.User, userRepo models.UserRepository) (*models.User, error) {
	password, err := HashPassword(userModel.Password)
	if err != nil {
		return nil, err
	}
	userModel.Password = password
	err = userRepo.Create(ctx, userModel)
	if err != nil {
		return nil, err
	}
	return userModel, nil
}
