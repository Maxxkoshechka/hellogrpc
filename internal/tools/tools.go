package tools

import (
	"encoding/json"
	"io"
)

func DataDecode(data io.Reader, model interface{}) error {
	decoder := json.NewDecoder(data)
	decoder.UseNumber()
	err := decoder.Decode(&model)
	return err
}
