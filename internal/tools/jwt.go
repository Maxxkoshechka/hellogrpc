package tools

import (
	"HelloWorld/internal/models"
	"github.com/dgrijalva/jwt-go"
	"os"
)

func CreateJWT(user *models.User) (string, error) {
	mySigningKey := []byte(os.Getenv("JWT_SECRET"))
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id":    user.Id,
		"name":  user.Name,
		"email": user.Email,
		"login": user.Login,
		"nbf":   user.CreatedAt,
	})
	tokenString, err := token.SignedString(mySigningKey)
	return tokenString, err
}
