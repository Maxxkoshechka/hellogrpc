package controllers

import (
	"HelloWorld/internal/models"
	"HelloWorld/internal/tools"
	pb "HelloWorld/proto"
	"context"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/timestamppb"
	"log"
	"net/http"
	"time"
)

type loginHandler struct {
	grpcConn grpc.ClientConn
}

func NewLoginHandler(grpcConn *grpc.ClientConn) *loginHandler {
	return &loginHandler{
		grpcConn: *grpcConn,
	}
}

func (h *loginHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	userModel := new(models.User)
	err := tools.DataDecode(r.Body, &userModel)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, "Bad request data")
		return
	}

	c := pb.NewHelloWorldClient(&h.grpcConn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	req, err := c.CreateUser(ctx, &pb.UserRequest{
		Name:      userModel.Name,
		Login:     userModel.Login,
		Email:     userModel.Email,
		Password:  userModel.Password,
		IsDeleted: userModel.IsDeleted,
		CreatedAt: timestamppb.Now(),
		UpdatedAt: timestamppb.Now(),
	})
	if err != nil {
		log.Fatalf("User create error: %v", err)
	}
	log.Printf("Message: %s", req.Success)
}
