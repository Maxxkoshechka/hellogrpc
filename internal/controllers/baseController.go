package controllers

import (
	pb "HelloWorld/proto"
	"context"
	"google.golang.org/grpc"
	"log"
	"net/http"
	"time"
)

type baseHandler struct {
	grpcConn grpc.ClientConn
}

func NewBaseHandler(grpcConn *grpc.ClientConn) *baseHandler {
	return &baseHandler{
		grpcConn: *grpcConn,
	}
}

func (h *baseHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Println(r.URL)

	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	c := pb.NewHelloWorldClient(&h.grpcConn)

	message := r.URL.Query().Get("message")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	req, err := c.SayHello(ctx, &pb.Request{Hello: message})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	log.Printf("Message: %s", req.GetMessage())
}
