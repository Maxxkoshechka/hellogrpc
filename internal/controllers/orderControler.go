package controllers

import (
	"HelloWorld/internal/models"
	"HelloWorld/internal/tools"
	pb "HelloWorld/proto"
	"context"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/timestamppb"
	"log"
	"net/http"
	"time"
)

type orderHandler struct {
	grpcConn grpc.ClientConn
}

func NewOrderHandler(grpcConn *grpc.ClientConn) *orderHandler {
	return &orderHandler{
		grpcConn: *grpcConn,
	}
}

func (h *orderHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	c := pb.NewHelloWorldClient(&h.grpcConn)

	orderModel := new(models.Order)

	err := tools.DataDecode(r.Body, &orderModel)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, "Bad request data")
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	req, err := c.CreateOrder(ctx, &pb.OrderRequest{
		UserId: &pb.User{
			UserId: int32(orderModel.UserID),
			Name:   "Гость",
		},
		Amount:      int32(orderModel.Amount),
		Description: orderModel.Description,
		Comments:    orderModel.Comments,
		Status:      orderModel.Status,
		CreatedAt:   timestamppb.New(orderModel.CreatedAt),
		UpdatedAt:   timestamppb.New(orderModel.UpdatedAt),
	})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	log.Printf("Message: %s", req.Success)
}
