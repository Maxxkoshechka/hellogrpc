package models

import (
	"context"
	"time"
)

type Order struct {
	Id          int
	UserID      int
	Amount      int
	Description string
	Comments    string
	Status      string
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

type OrderRepository interface {
	Create(ctx context.Context, order *Order) error
	//Select(query string, params ...interface{}) (*Order, error)
	Update(order *Order, query string) (*Order, error)
	Delete(order *Order) error
}
