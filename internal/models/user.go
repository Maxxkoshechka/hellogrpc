package models

import (
	"context"
	"errors"
	"net/mail"
	"regexp"
	"time"
)

type User struct {
	Id        int
	Name      string
	Login     string
	Email     string
	Password  string
	IsDeleted bool
	CreatedAt time.Time
	UpdatedAt time.Time
}

type UserRepository interface {
	Create(ctx context.Context, user *User) error
	Select(ctx context.Context, query string, params ...interface{}) (*User, error)
	Update(user *User, query string) (*User, error)
	Delete(user *User) error
}

func (u *User) ValidateUserData() error {
	loginPattern := `^[a-zA-Z]{1}[\w@\.]{5,}$`
	namePattern := `^[a-zA-Z]{3,}$`
	if u.Login != "" {
		matched, err := regexp.MatchString(loginPattern, u.Login)
		if err != nil {
			return err
		} else if !matched {
			return errors.New("login error")
		}
	} else {
		return errors.New("login is empty")
	}
	if u.Name != "" {
		matched, err := regexp.MatchString(namePattern, u.Name)
		if err != nil {
			return err
		} else if !matched {
			return errors.New("name error")
		}
	}
	if u.Email != "" {
		_, err := mail.ParseAddress(u.Email)
		if err != nil {
			return err
		}
	}
	return nil
}
