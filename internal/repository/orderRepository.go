package repository

import (
	"HelloWorld/internal/models"
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"os"
)

type orderRepo struct {
	conn *pgxpool.Pool
}

func NewOrderRepo(conn *pgxpool.Pool) models.OrderRepository {
	return &orderRepo{
		conn: conn,
	}
}

func (o orderRepo) Create(ctx context.Context, order *models.Order) error {
	err := o.conn.QueryRow(ctx, "insert into orders "+
		"(user_id, amount, description, comments, status, created_at, updated_at) "+
		"values ($1, $2, $3, $4, $5, $6, $7) RETURNING id",
		order.UserID,
		order.Amount,
		order.Description,
		order.Comments,
		order.Status,
		order.CreatedAt,
		order.UpdatedAt,
	).Scan(&order.Id)
	if err != nil {
		fmt.Fprintf(os.Stderr, "QueryRow failed: %v\n", err)
	}
	return nil
}

func (o *orderRepo) Update(order *models.Order, query string) (*models.Order, error) {
	return &models.Order{}, nil
}

func (o *orderRepo) Delete(order *models.Order) error {
	return nil
}
