package repository

import (
	"HelloWorld/internal/models"
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"os"
)

type userRepo struct {
	conn *pgxpool.Pool
}

func NewUserRepo(conn *pgxpool.Pool) models.UserRepository {
	return &userRepo{
		conn: conn,
	}
}

func (r *userRepo) Select(ctx context.Context, query string, params ...interface{}) (*models.User, error) {

	user := new(models.User)
	err := r.conn.QueryRow(ctx, query, params...).Scan(
		&user.Id, &user.Name, &user.Email, &user.Login, &user.Password, &user.CreatedAt, &user.IsDeleted)
	if err != nil {
		fmt.Fprintf(os.Stderr, "QueryRow failed: %v\n", err)
		return nil, err
	}
	return user, nil
}

func (r *userRepo) Create(ctx context.Context, user *models.User) error {
	err := r.conn.QueryRow(context.Background(), "insert into users "+
		"(name, email, login, password) values ($1, $2, $3, $4) RETURNING id",
		user.Name,
		user.Email,
		user.Login,
		user.Password,
	).Scan(&user.Id)
	if err != nil {
		fmt.Fprintf(os.Stderr, "QueryRow failed: %v\n", err)
	}
	return nil
}

func (r *userRepo) Update(user *models.User, query string) (*models.User, error) {
	return &models.User{}, nil
}

func (r *userRepo) Delete(user *models.User) error {
	return nil
}
