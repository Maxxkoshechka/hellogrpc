package server

import (
	"HelloWorld/internal/service"
	pb "HelloWorld/proto"
	"context"
	"log"
)

type GrpcServer struct {
	order service.Order
	user  service.User
	pb.UnimplementedHelloWorldServer
}

func NewGrpcServer(order service.Order, user service.User) *GrpcServer {
	return &GrpcServer{
		order: order,
		user:  user,
	}
}

func (s *GrpcServer) SayHello(ctx context.Context, in *pb.Request) (*pb.Response, error) {
	log.Printf("Received: %v", in.GetHello())
	return &pb.Response{Message: "Hello " + in.GetHello()}, nil
}

func (s *GrpcServer) CreateOrder(ctx context.Context, r *pb.OrderRequest) (*pb.BaseResponse, error) {
	err := s.order.CreateOrder(ctx, r)
	if err != nil {
		return nil, err
	}
	return &pb.BaseResponse{Success: true, Error: ""}, nil
}

func (s *GrpcServer) CreateUser(ctx context.Context, r *pb.UserRequest) (*pb.BaseResponse, error) {
	err := s.user.CreateUser(ctx, r)
	if err != nil {
		return &pb.BaseResponse{Success: false, Error: err.Error()}, err
	}
	return &pb.BaseResponse{Success: true, Error: ""}, nil
}
